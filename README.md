CREATE PROC GetAllInvoices
AS
Select * from Invoices;


CREATE PROC GetProductsByInvoice
@InvoiceId int
AS
Select Products.ProductId, Products.Name ,InvoicesAndProducts.Quantity from InvoicesAndProducts
where(InvoiceId = InvoiceId)
INNER JOIN Products ON InvoicesAndProducts.ProductId=Products.ProductId;
 
CREATE PROC GetClient
@ClientId int
AS
Select * from Clients
where ClientId = @ClientId;


CREATE PROC GetAllClient
AS
Select * from Clients;


CREATE PROC GetInvoicesAndProducts
@InvoicesAndProductsId
AS
Select * from InvoicesAndProducts
where InvoicesAndProductsId = @InvoicesAndProductsId;


CREATE PROC GetAllInvoicesAndProducts
AS
Select * from InvoicesAndProducts;

CREATE PROC GetAllProducts
AS
Select * from InvoicesAndProducts;




CREATE PROC CreateInvoice
@DateExpired datetime,
@Owner varchar(100),
@NumberItems int,
@BasePrice int,
@Tax FLOAT,
@Total FLOAT,
@Observaction VARCHAR(300),
@ClientId int
AS
INSERT INTO Invoices ( DateExpired , Owner ,NumberItems, BasePrice  ,Tax ,Total,  Observaction,  ClientId)
Values( @DateExpired, @Owner,@NumberItems,@BasePrice, @Tax, @Total, @Observaction, @ClientId );


CREATE PROC CreateClient
@Name varchar(100),
@Observaction VARCHAR(300)
AS
INSERT INTO Clients (Name, Observaction)
Values( @Name,  @Observaction);



CREATE PROC CreateInvoiceProducts
@Quantity float, 
@Observaction VARCHAR(300),
@InvoiceId INT,
@ProductId INT 
AS
INSERT INTO InvoicesAndProducts (Quantity, Observaction,InvoiceId, ProductId)
Values( @Quantity,  @Observaction,@InvoiceId,@ProductId);


CREATE PROC CreatedProduct
@Name VARCHAR (100),
@BasePrice FLOAT,
@Tax FLOAT,
@Observaction VARCHAR(300)
AS
INSERT INTO Products (Name, BasePrice, Tax,Observaction )
Values( @Name,@BasePrice,@Tax,@Observaction);
