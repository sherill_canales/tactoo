﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace UNAH_Test.Models
{
    public class Product
    {
        [DisplayName("Id")]
        public int ProductId { get; set; }
        [DisplayName("Fecha de creación")]
        public DateTime DateCreated { get; set; }
        [DisplayName("Nombre")]
        public string Name { get; set; }
        [DisplayName("Precio Base")]
        public float BasePrice { get; set; }
        [DisplayName("Impuestos")]
        public float Tax { get; set; }
        [DisplayName("Observación")]
        public string Observaction { get; set; }
    }
}