﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace UNAH_Test.Models
{
    public class Client
    {
        [DisplayName("Id")]
        public int ClientId { get; set; }
        [DisplayName("Nombre")]
        public string Name { get; set; }
        [DisplayName("Observación")]
        public string Observaction { get; set; }
    }
}