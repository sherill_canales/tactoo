﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace UNAH_Test.Models
{
    public class Invoice
    {
        [DisplayName("id")]
        public int InvoiceId { get; set; }
        [DisplayName("Fecha de la creación")]
        public DateTime DateCreated { get; set; }
        [DisplayName("Fecha de expiración")]
        public DateTime DateExpired { get; set; }
        [DisplayName("Numero de items")]
        public int NumberItems { get; set; }
        [DisplayName("Precio Base")]
        public float BasePrice { get; set; }
        [DisplayName("Impuestos")]
        public float Tax { get; set; }
        [DisplayName("Total")]
        public float  Total { get; set; }
        [DisplayName("Asociado")]
        public string Owner { get; set; }
        [DisplayName("Nombre")]
        public string Name { get; set; }
        [DisplayName("Observación")]
        public string Observaction { get; set; }
        [DisplayName("Cliente")]
        public int ClientId { get; set; }
        public Client Client { get; set; }
        public List<Product> Products { get; set; }

    }
}

 