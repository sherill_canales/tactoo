﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace UNAH_Test.Models
{
    public class InvoicesAndProduct
    {
        [DisplayName("id")]
        public int InvoicesAndProductsId { get; set; }
        [DisplayName("Cantidad")]
        public float Quantity { get; set; }
        [DisplayName("Observación")]
        public string Observaction { get; set; }
        [DisplayName("Factura")]
        public int InvoiceId { get; set; }
        [DisplayName("Producto")]
        public int ProductId { get; set; }
    }
}