﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(UNAH_Test.Startup))]
namespace UNAH_Test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
