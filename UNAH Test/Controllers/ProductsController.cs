﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using UNAH_Test.Models;

namespace UNAH_Test.Controllers
{
    public class ProductsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Products
        public ActionResult Index()
        {
            List<Product> Products = new List<Product>() {
                new Product() {Name = "Asesoramiento", BasePrice = 1 , DateCreated =DateTime.Now, Observaction = "",
                ProductId =1,
                Tax = 34
                }
            };

            return View(Products);
        }

 
        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

 
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductId,DateCreated,Name,BasePrice,Tax,Observaction")] Product product)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(product);
        }

 
    }
}
