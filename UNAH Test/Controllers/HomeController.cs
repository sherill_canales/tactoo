﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UNAH_Test.Models;

namespace UNAH_Test.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<Invoice> Invoices = new List<Invoice>() {
                new Invoice { InvoiceId = 1, Client = new Client (){ Name = "Luis Martinez"}, DateCreated = DateTime.Now}
            };

 
            return View(Invoices);
        }

 
    }
}